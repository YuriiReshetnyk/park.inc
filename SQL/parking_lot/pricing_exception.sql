CREATE TABLE parking_lot.pricing_exception (
	pricing_exception_id SERIAL PRIMARY KEY
	, parking_slot_id INT NOT NULL
	, day_of_week days_of_week[] NOT NULL
	, morning_hr_cost INT NOT NULL
	, midday_hr_cost INT NOT NULL
	, evening_hr_cost INT NOT NULL
	, all_day_cost INT NOT NULL
	, FOREIGN KEY(parking_slot_id) REFERENCES parking_lot.parking_slot(parking_slot_id) ON DELETE CASCADE
);