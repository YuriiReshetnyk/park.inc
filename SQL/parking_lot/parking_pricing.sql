CREATE TABLE parking_lot.parking_pricing (
	parking_pricing_id SERIAL PRIMARY KEY
	, parking_slot_id INT NOT NULL
	, morning_hr_cost INT NOT NULL
	, midday_hr_cost INT NOT NULL
	, evening_hr_cost INT NOT NULL
	, all_day_cost INT NOT NULL
	, FOREIGN KEY(parking_slot_id) REFERENCES parking_lot.parking_slot(parking_slot_id) ON DELETE CASCADE
);