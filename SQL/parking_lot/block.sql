CREATE TABLE parking_lot.block (
	block_id SERIAL PRIMARY KEY
	, parking_lot_id INT NOT NULL
	, block_code VARCHAR(3) NOT NULL
	, number_of_floors INT NOT NULL
	, is_block_full BOOLEAN DEFAULT FALSE
	, FOREIGN KEY(parking_lot_id) REFERENCES parking_lot.parking_lot(parking_lot_id) ON DELETE CASCADE
);