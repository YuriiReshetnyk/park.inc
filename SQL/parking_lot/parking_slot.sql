CREATE TABLE parking_lot.parking_slot(
	parking_slot_id SERIAL PRIMARY KEY
	, floor_id INT NOT NULL
	, slot_number INT NOT NULL
	, wing_code CHAR(1) NOT NULL
	, owner_id INT DEFAULT NULL
	, ownership_documents VARCHAR(255)
	, FOREIGN KEY(owner_id) REFERENCES person.person(person_id) ON DELETE CASCADE
	, FOREIGN KEY(floor_id) REFERENCES parking_lot.floors(floor_id) ON DELETE CASCADE
); 