CREATE TABLE parking_lot.slot_comments
(
    slot_comments_id SERIAL PRIMARY KEY
    , parking_slot INT NOT NULL
    , writer_id INT NOT NULL
    , slot_comment VARCHAR(1000) NOT NULL
    , FOREIGN KEY(parking_slot) REFERENCES parking_lot.parking_slot(parking_slot_id) ON DELETE CASCADE
    , FOREIGN KEY(writer_id) REFERENCES person.person(person_id) ON DELETE CASCADE
);