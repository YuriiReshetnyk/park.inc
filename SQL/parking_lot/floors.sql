CREATE TABLE parking_lot.floors (
	floor_id SERIAL PRIMARY KEY
	, block_id INT NOT NULL
	, floor_number INT NOT NULL -- when 1 = ground level
	, max_height_in_inch INT NOT NULL
	, number_of_wings INT NOT NULL
	, number_of_slots INT NOT NULL
	, is_floor_full BOOLEAN DEFAULT FALSE
	, FOREIGN KEY(block_id) REFERENCES parking_lot.block(block_id) ON DELETE CASCADE
);