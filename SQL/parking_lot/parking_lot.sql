CREATE TABLE parking_lot.parking_lot(
	parking_lot_id SERIAL PRIMARY KEY
	, number_of_blocks INT NOT NULL
	, is_slot_available BOOLEAN DEFAULT TRUE
	, address VARCHAR(255) NOT NULL
	, operating_company_name VARCHAR(100) DEFAULT NULL
	, is_reentry_allowed BOOLEAN DEFAULT FALSE
	, operating_in_night BOOLEAN DEFAULT FALSE
);