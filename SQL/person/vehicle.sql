CREATE TABLE person.vehicle 
(
 	vehicle_id SERIAL PRIMARY KEY
	, owner_id INT NOT NULL
	, vehicle_number VARCHAR(20) NOT NULL
	, FOREIGN KEY(owner_id) REFERENCES person.person(person_id) ON DELETE CASCADE
);