CREATE TABLE person.person
(
	person_id SERIAL PRIMARY KEY
	, email VARCHAR(30) UNIQUE
	, passwords VARCHAR(30) NOT NULL
	, first_name VARCHAR(20) NOT NULL
	, last_name VARCHAR(20) NOT NULL
	, phone_number VARCHAR(13) NOT NULL
	, photo VARCHAR(50) NOT NULL
	, languages language_type DEFAULT 'Ukrainian'
	, notifications BOOLEAN DEFAULT True
	, tips BOOLEAN DEFAULT True
	, theme theme_type DEFAULT 'blue'
	, location_access BOOLEAN DEFAULT False
	, camera_access BOOLEAN DEFAULT False
	, contact_access BOOLEAN DEFAULT False
);