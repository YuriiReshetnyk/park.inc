CREATE TABLE parking_slip
(
    parking_slip_id SERIAL PRIMARY KEY
    , parking_reservation_id INT NOT NULL
    , actual_entry_time TIMESTAMP NOT NULL
    , actual_exit_time TIMESTAMP NOT NULL
    , basic_cost INT NOT NULL
    , penalty INT NOT NULL
    , total_cost INT NOT NULL
    , is_paid BOOLEAN DEFAULT FALSE
    , FOREIGN KEY(parking_reservation_id) REFERENCES parking_reservation(parking_reservation_id) ON DELETE CASCADE
);