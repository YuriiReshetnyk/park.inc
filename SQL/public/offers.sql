CREATE TABLE parking_lot.offers
(
	offers_id SERIAL PRIMARY KEY
	, parking_slot_id INT NOT NULL
	, issued_on DATE NOT NULL
	, vallied_till DATE NOT NULL
	, days_of_week parking_lot.days_of_week[] NOT NULL
	, start_time TIME NOT NULL
	, end_time TIME NOT NULL
	, FOREIGN KEY(parking_slot_id) REFERENCES parking_lot.parking_slot(parking_slot_id) ON DELETE CASCADE
);