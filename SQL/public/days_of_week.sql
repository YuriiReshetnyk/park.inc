CREATE TYPE days_of_week as ENUM
(
	'Monday'
	, 'Tuesday'
	, 'Wednesday'
	, 'Thursday'
	, 'Friday'
	, 'Saturday'
	, 'Sunday'
);