CREATE TYPE language_type AS ENUM
(
	'Ukrainian'
	, 'English'
);