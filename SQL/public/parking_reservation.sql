CREATE TABLE parking_reservation
(
	parking_reservation_id SERIAL PRIMARY KEY
	, customer_id INT NOT NULL
	, parking_slot_id INT NOT NULL
	, start_timestamp TIMESTAMP NOT NULL
	, duration_in_minutes INT NOT NULL
	, booking_date DATE NOT NULL
	, FOREIGN KEY(customer_id) REFERENCES person.person(person_id) ON DELETE CASCADE
	, FOREIGN KEY(parking_slot_id) REFERENCES parking_lot.parking_slot(parking_slot_id) ON DELETE CASCADE
);