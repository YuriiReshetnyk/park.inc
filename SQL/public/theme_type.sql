CREATE TYPE theme_type AS ENUM
(
	'black'
	, 'white'
	, 'blue'
	, 'green'
);