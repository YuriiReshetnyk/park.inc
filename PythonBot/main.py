import time
import telebot
from telebot import types
import psycopg2

host = "ella.db.elephantsql.com"
user = "yxqfwklx"
password = "cWmYO0EkdU_nDVvGgWXKsJEf4RQLSR8F"
db_name = "yxqfwklx"

available_time = [['10:00', '11:00'], ['11:00', '12:00'], ['15:00', '16:00'], ['16:00', '17:00']]
chosen_time = []
place_number = 0
PAYMENTS_TOKEN = '632593626:TEST:sandbox_i23945637791'
bot = telebot.TeleBot('2035715598:AAEEqh8uNw8e0EvuUrQoWNpqmMibhwo8PSs')


@bot.message_handler(commands=['db'])
def db(message):
    try:
        connection = psycopg2.connect(
            host=host,
            user=user,
            password=password,
            database=db_name
        )

        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT version();"
            )

            print(f"Server version: {cursor.fetchone()}")

    except Exception as _ex:
        print("[INFO] Error", _ex)
    finally:
        if connection:
            connection.close()
            print("[INFO] PostgreSQL connection close")


@bot.message_handler(commands=['buy'])
def buy(message):
    lab2 = types.LabeledPrice('this2', 15000)
    bot.send_invoice(chat_id=message.chat.id, title='paytest', description='test payment', invoice_payload='good1',
                     provider_token=PAYMENTS_TOKEN, currency='UAH', start_parameter='test_bot', prices=[lab2])


@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True,
                                  error_message='Щось пішло не так :(')


@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    bot.send_message(message.chat.id, 'Оплата пройшла успішно!')
    bot.send_message(message.chat.id, 'Перейдіть у розділ "Мої замовлення", щоб відкрити ворота')
    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(types.InlineKeyboardButton('Знайти паркомісце', callback_data='find_place'),
               types.InlineKeyboardButton('Запропонувати своє місце', callback_data='offer_place'),
               types.InlineKeyboardButton('Мої замовлення', callback_data='orders'))
    bot.send_message(message.chat.id, 'Виберіть потрібну вам опцію', reply_markup=markup)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, 'Я чат бот проекту Park.inc')
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(types.KeyboardButton('Надіслати контакти', request_contact=True))
    bot.send_message(message.chat.id, 'Щоб продовжити, надішліть, будь ласка, свої контакти.'
                                      ' Вони нам потрібні, щоб зареєструвати вас у нашій системі', reply_markup=markup)


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id,
                     'За допомогою цього бота ви можете знаходити за допомогою карти паркомісця та винаймати їх')


@bot.message_handler(content_types=['contact'])
def get_contact(message):

    msg = bot.send_message(message.chat.id, 'Дякую! Контакти отримано', reply_markup=types.ReplyKeyboardRemove())
    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(types.InlineKeyboardButton('Знайти паркомісце', callback_data='find_place'),
               types.InlineKeyboardButton('Запропонувати своє місце', callback_data='offer_place'),
               types.InlineKeyboardButton('Мої замовлення', callback_data='orders'))
    bot.send_message(message.chat.id, 'Виберіть потрібну вам опцію', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def inline_buttons(call):
    global place_number
    if call.data == 'find_place':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Виберіть потрібну вам опцію', reply_markup=None)
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.add(types.KeyboardButton('Надіслати геолокацію', request_location=True),
                   types.KeyboardButton('Вибрати місце самостійно'))
        msg = bot.send_message(call.message.chat.id,
                               'Ви можете надіслати свою геолокацію, щоб побачити паркомісця поблизу або самостійно вибрати місце. Зауважте, що ви не зможете надіслати геолокацію, якщо використовуєте комп`ютер.',
                               reply_markup=markup)
    if call.data == 'that':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Виберіть час на який ви б хотіли винайняти паркомісце', reply_markup=None)
        bot.send_message(call.message.chat.id, 'Будь ласка, пройдіть оплату')
        time.sleep(5)
        bot.send_message(call.message.chat.id,
                         'Дякую! Щоб переглянути свої замовлення або відчитини ворота до паркінгу натисніть "Мої замовлення".')
        time.sleep(2)
        markup1 = types.InlineKeyboardMarkup(row_width=1)
        markup1.add(types.InlineKeyboardButton('Знайти паркомісце', callback_data='find_place'),
                    types.InlineKeyboardButton('Запропонувати своє місце', callback_data='offer_place'),
                    types.InlineKeyboardButton('Мої замовлення', callback_data='orders'))
        bot.send_message(call.message.chat.id, 'Виберіть потрібну вам опцію', reply_markup=markup1)
    if call.data == 'orders':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Виберіть потрібну вам опцію', reply_markup=None)
        markup2 = types.InlineKeyboardMarkup()
        markup2.add(types.InlineKeyboardButton('Повернутися назад', callback_data='return'))
        if place_number == 0:
            bot.send_message(call.message.chat.id, 'У вас немає замовлень', reply_markup=markup2)
        else:
            bot.send_message(call.message.chat.id, 'Ось список ваших замовлень:')
            markup3 = types.InlineKeyboardMarkup(row_width=1)
            markup3.add(types.InlineKeyboardButton('Відкрити ворота', callback_data='open_gates'),
                        types.InlineKeyboardButton('Повернутися назад', callback_data='return'))
            bot.send_message(call.message.chat.id, f'Паркомісце {place_number}', reply_markup=markup3)
    if call.data == 'open_gates':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f'Паркомісце {place_number}')
        time.sleep(3)
        markup6 = types.InlineKeyboardMarkup()
        markup6.add(types.InlineKeyboardButton('Повернутися назад', callback_data='return'))
        bot.send_message(call.message.chat.id, 'Ворота відчинилися!', reply_markup=markup6)
        time.sleep(3)
    if call.data == 'return':
        markup5 = types.InlineKeyboardMarkup(row_width=1)
        markup5.add(types.InlineKeyboardButton('Знайти паркомісце', callback_data='find_place'),
                    types.InlineKeyboardButton('Запропонувати своє місце', callback_data='offer_place'),
                    types.InlineKeyboardButton('Мої замовлення', callback_data='orders'))
        bot.send_message(call.message.chat.id, 'Виберіть потрібну вам опцію', reply_markup=markup5)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Повернутися назад', reply_markup=None)
    if call.data == 'offer_place':
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Виберіть потрібну вам опцію', reply_markup=None)
        markup7 = types.InlineKeyboardMarkup()
        markup7.add(types.InlineKeyboardButton('Повернутися назад', callback_data='return'))
        bot.send_message(call.message.chat.id,
                         'Щоб здати в оренду своє паркомісце, вам необхідно відвідати наш сайт, розділ "Співпраця"',
                         reply_markup=markup7)


@bot.message_handler(content_types=['text'])
def choose_place_by_yourself(message):
    if message.text == 'Вибрати місце самостійно':
        bot.send_message(message.chat.id, 'Вам потрібно буде обрати паркомісце на карті',
                         reply_markup=types.ReplyKeyboardRemove())
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text='Відкрити карту', web_app=types.WebAppInfo(
            'https://umap.openstreetmap.fr/uk-ua/map/map_758045#17')))
        bot.send_message(message.chat.id, 'Ось карта:',
                         reply_markup=markup)
        time.sleep(6)
        msg = bot.send_message(message.chat.id, 'Вже знайшли потрібне місце? Напишіть його номер')
        bot.register_next_step_handler(msg, choose_place)


@bot.message_handler(content_types=['location'])
def get_location(message):
    global lat, lon
    lat = message.location.latitude
    lon = message.location.longitude
    bot.send_message(message.chat.id, 'Дякую! Вашу геолокацію отримано', reply_markup=types.ReplyKeyboardRemove())
    markup = types.InlineKeyboardMarkup()
    web_app = types.WebAppInfo(f'https://umap.openstreetmap.fr/uk-ua/map/map_758045#15/{lat}/{lon}')
    markup.add(types.InlineKeyboardButton('Карта', web_app=web_app))
    bot.send_message(message.chat.id, 'Натисніть на кнопку нижче щоб побачити найближчі паркомісця на карті',
                     reply_markup=markup)
    msg = bot.send_message(message.chat.id, 'Вже знайшли потрібне місце? Напишіть його номер')
    bot.register_next_step_handler(msg, choose_place)


def choose_place(message):
    global place_number, available_time
    place_number = int(message.text)
    markup = types.ReplyKeyboardMarkup(row_width=2)
    for i in range(len(available_time)):
        markup.add(types.KeyboardButton(f'{available_time[i][0]}-{available_time[i][1]}'))
    msg = bot.send_message(message.chat.id, f'Виберіть час на який ви б хотіли винайняти паркомісце №{place_number}.',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, choose_time)


def choose_time(message):
    time = str(message.text)
    if time != 'Перейти до оплати':
        global available_time, chosen_time
        markup = types.ReplyKeyboardMarkup(row_width=1)
        available_time_of_place = available_time
        available_time_of_place.remove([f'{time[0:5]}', f'{time[6:11]}'])
        chosen_time.append([f'{time[0:5]}', f'{time[6:11]}'])
        for i in range(len(available_time)):
            markup.add(types.KeyboardButton(f'{available_time_of_place[i][0]}-{available_time_of_place[i][1]}'))
        markup.add(types.KeyboardButton('Перейти до оплати'))
        msg = bot.send_message(message.chat.id, 'Виберіть всі потрібні вам години або перейдіть до оплати',
                               reply_markup=markup)
        bot.register_next_step_handler(msg, choose_time)
    else:
        global place_number
        bot.send_message(message.chat.id, 'Ось список ваших замовлень:', reply_markup=types.ReplyKeyboardRemove())
        markup1 = types.InlineKeyboardMarkup(row_width=1)
        bot.send_message(message.chat.id, f'Паркомісце {place_number}', reply_markup=markup1)
        chosen_time_string = ''
        list_of_labels = []
        for i in chosen_time:
            list_of_labels.append(types.LabeledPrice(f'{i[0]}-{i[1]}', 1500))
            chosen_time_string = chosen_time_string + i[0] + '-' + i[1] + ';' + ' '
        bot.send_message(message.chat.id, f'{chosen_time_string}')
        bot.send_invoice(chat_id=message.chat.id, title='Оплата', description='Пройдіть оплату',
                         invoice_payload='payment',
                         provider_token=PAYMENTS_TOKEN, currency='UAH', start_parameter='test_bot',
                         prices=list_of_labels)


bot.polling(none_stop=True)
